package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {

    public static WebDriverWait oneMinuteWaiter;

    public static WebDriverWait HomePageLoadingWaiter(WebDriver driver){
        oneMinuteWaiter = new WebDriverWait(driver, 60);
        return oneMinuteWaiter;
    }
}

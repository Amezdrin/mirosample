package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

public class WebDriverInitializer {

    public WebDriver Chrome;

    public WebDriver initializeChromeDriver(){
        File chromeDriver = new File("webdrivers/chromedriver");
        System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");
        Chrome = new ChromeDriver(options);
        return Chrome;
    }
}

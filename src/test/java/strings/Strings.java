package strings;

public class Strings {

    /** Credentials */
    public static String validEmail = "<this email has been hidden>"; //use your own email 
    public static String validPwd = "<this password has been hidden>"; // use your own pwd
    public static String domain = "@gmail.com";

    /** Error Messages*/
    public static String invalidCredentialsError = "The email or password you entered is incorrect.\n" +
            "Please try again.";

    /** URLs */
    public static String homePageURL = "https://miro.com/login/";
}


package chrome;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.PageSignIn;
import utils.WebDriverInitializer;

import static strings.Strings.*;


public class TestSignIn {

    WebDriver driver;
    WebDriverInitializer webDriverInitializer = new WebDriverInitializer();
    PageSignIn pageSignIn;

    @Before
    public void preconditons(){
        driver = webDriverInitializer.initializeChromeDriver();
        pageSignIn = new PageSignIn(driver);
        driver.get(homePageURL);
        pageSignIn.checkElementPresence();
    }

    @Test
    public void PositiveSignInflow(){
        pageSignIn.loginValidUser(validEmail+domain, validPwd);
    }

    @Test
    public void NegativeSignInflow(){
        pageSignIn.loginInvalidUser(domain, validPwd);
    }

    @After
    public void postcondition(){
        driver.quit();
    }
}

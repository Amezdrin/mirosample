package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;
import static strings.Strings.*;
import static utils.Waiter.HomePageLoadingWaiter;


public class PageSignIn {

    protected WebDriver driver;
    String buffer;

    public PageSignIn(WebDriver driver){
        this.driver = driver;
    }

    private By emailBy = By.name("email");
    private By passwordBy = By.name("password");
    private By signinBy = By.className("signup__submit");
    private By signUpRecovery = By.className("signup__recovery");
    private By signInErrorMessage = By.className("signup__error-item");
    private By signUpButton = By.className("overlay-signup__btn");
    private By signInWithSSOButton = By.className("signup__under-submit");
    private By visibleDashboard = By.className("rtb-btn--secondary-just-bordered");
    private By userProfileBtn = By.className("user-profile");

    /** positive login func */
    public void loginValidUser(String email, String password) {
        driver.findElement(emailBy).sendKeys(email);
        driver.findElement(passwordBy).sendKeys(password);
        driver.findElement(signinBy).click();
        HomePageLoadingWaiter(driver)
                .until(visibilityOfElementLocated
                        (visibleDashboard)).isDisplayed();
        assertTrue(driver.findElement(userProfileBtn).isDisplayed());
    }

    /** negative login func */
    public void loginInvalidUser(String email, String password){
        driver.findElement(emailBy).sendKeys(email);
        driver.findElement(passwordBy).sendKeys(password);
        driver.findElement(signinBy).click();
        HomePageLoadingWaiter(driver)
                .until(visibilityOfElementLocated
                        (signInErrorMessage)).isDisplayed();
        buffer = driver.findElement(signInErrorMessage).getText();
        assertEquals(buffer, invalidCredentialsError);
    }

    /** elements checker func for preconditons */
    public void checkElementPresence(){
        assertTrue(driver.findElement(emailBy).isDisplayed());
        assertTrue(driver.findElement(passwordBy).isDisplayed());
        assertTrue(driver.findElement(signinBy).isDisplayed());
        assertTrue(driver.findElement(signUpRecovery).isDisplayed());
        assertTrue(driver.findElement(signUpButton).isDisplayed());
        assertTrue(driver.findElement(signInWithSSOButton).isDisplayed());
    }

}
# MiroSample

Prepare automated test for auth at https://miro.com/login/. Please exclude social network and SSO auth.

Requirements:
- tests should be on Java or C#
- tests should be based on Selenium WebDriver.
